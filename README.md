# relations-primitives-2019

Important: 

**The code has been moved to: https://gitlab.irit.fr/melodi/andiamo/discourserelations/relations-primitives-2019**

Code for the paper:

Charlotte Roze, Chloé Braud, Philippe Muller. Which aspects of discourse relations are hard to learn? Primitive decomposition for discourse relation classification. Annual SIGdial Meeting on Discourse and Dialogue, Sep 2019.
